import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { MasterProvider } from "../../providers/master/master";
/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  public pass:string;
  public name:string;
  public email:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public master:MasterProvider) {
  }
  onSubmit() {
    this.master.register(this.name,this.email,this.pass).subscribe(data=>{
      console.log(data);
      this.navCtrl.push(LoginPage);
    })

    
    
 }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

}
