import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MasterProvider } from "../../providers/master/master";
import { UpdatePwPage } from "../update-pw/update-pw";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public Userdata:any;
  constructor(public navCtrl: NavController,public master: MasterProvider) {
    this.Getuser();
  }

  public Getuser(){
    this.master.GetAll().subscribe(data=>{
      console.log(data);
      this.Userdata=data;
    })
  }

  public Delete(id){
    this.master.Delete(id).subscribe(data=>{
      this.Getuser();
    });
    console.log(id);
  }

  GantiHal(){
    this.navCtrl.push(UpdatePwPage);
  }

}
