import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { MasterProvider } from "../../providers/master/master";

/**
 * Generated class for the UpdatePwPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-update-pw',
  templateUrl: 'update-pw.html',
})
export class UpdatePwPage {
  public email:any;
  public pass:any;
  public passbaru:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public master:MasterProvider) {
  }


  Ganti() {
    this.master.Changepass(this.email,this.pass,this.passbaru).subscribe(data=>{
      console.log(data);
      if(data.success){
        this.navCtrl.setRoot(LoginPage);
      }else{
        console.log("salah password");
      }
      
    });
    
    
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdatePwPage');
  }

}
