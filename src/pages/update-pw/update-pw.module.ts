import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpdatePwPage } from './update-pw';

@NgModule({
  declarations: [
    UpdatePwPage,
  ],
  imports: [
    IonicPageModule.forChild(UpdatePwPage),
  ],
})
export class UpdatePwPageModule {}
