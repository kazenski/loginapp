import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SignupPage } from '../signup/signup';
import { HomePage } from '../home/home';
import { MasterProvider } from "../../providers/master/master";




/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  public pass:any;
  public email:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public master:MasterProvider) {
  }
  goSignupPage() {
    this.navCtrl.push(SignupPage);
    
 }
 goHome() {
   this.master.Login(this.email,this.pass).subscribe(data=>{
     if(data.success){  
      this.navCtrl.push(HomePage);
     }else{
       console.log('Salah Credentials');
     };
   });
  
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
