import { Injectable }                   from '@angular/core';
import { Http, Headers, Response, RequestOptions }      from '@angular/http';
import { Observable }                   from 'rxjs/Observable';
import 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

/*
  Generated class for the MasterProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
// declare var username;
// declare var password;

@Injectable()
export class MasterProvider {

  constructor(public http: Http) {
    
    console.log('Hello MasterProvider Provider');
  }
  // public static username = 'kzen';
  // public static password = 'kzen';

  public static api_url = 'http://localhost/logins/public/user';


  register(nama,email,password):Observable<any>{
    let headers = new Headers(); 
    let body = {
        name: nama,
        password:password,
        email: email,
    }
    headers.append('Content-Type','application/json');  
    return this .http 
        .post(MasterProvider.api_url+"/register", JSON.stringify(body),{headers:headers}) 
        .map((data) => { 
            return data.json();
        }) 
  }


  GetAll():Observable<any>{
    
    return this .http 
        .get(MasterProvider.api_url+"/all") 
        .map((response: Response) =>
        {
            return response.json();
        });
  }

  Delete(id):Observable<any>{
    let headers = new Headers(); 
    let body = {
        id: id
    }
    headers.append('Content-Type','application/json');  
    return this .http 
        .post(MasterProvider.api_url+"/delete", JSON.stringify(body),{headers:headers}) 
        .map((data) => { 
            return data.json();
        }) 
  }

  Changepass(email,pass,passbaru):Observable<any>{
    let headers = new Headers(); 
    let body = {
        email:email,
        pass:pass,
        passbaru:passbaru
    }
    headers.append('Content-Type','application/json');  
    return this .http 
        .post(MasterProvider.api_url+"/update", JSON.stringify(body),{headers:headers}) 
        .map((data) => { 
            return data.json();
        }) 
  }

  Login(email,pass):Observable<any>{
    let headers = new Headers(); 
    let body = {
        email:email,
        pass:pass
    }
    headers.append('Content-Type','application/json');  
    return this .http 
        .post(MasterProvider.api_url+"/login", JSON.stringify(body),{headers:headers}) 
        .map((data) => { 
            return data.json();
        }) 
  }

  
}
